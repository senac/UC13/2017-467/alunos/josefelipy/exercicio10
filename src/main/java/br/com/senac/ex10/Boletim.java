
package br.com.senac.ex10;

import java.util.List;


public class Boletim {

    int calculoDeAlunos(List<Aluno> lista) {
    int c = 0;
            for(Aluno aluno : lista){
                int nota = (int) (aluno.getNotaA() + aluno.getNotaB()) / 2;
            if(nota >= 7){
                c = c + 1;
            }
            }
            return c;
    }
    
}
