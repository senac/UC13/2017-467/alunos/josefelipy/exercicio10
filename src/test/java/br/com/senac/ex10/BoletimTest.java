
package br.com.senac.ex10;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class BoletimTest {
    
    public BoletimTest() {
        
    }
    
    @Test
    public void verificarQuantosAlunosEstaoAcimaDeSete(){
      Aluno jose = new Aluno("José", 9, 10);
      Aluno felipy = new Aluno("Felipy", 6, 7);
      Aluno fernando = new Aluno("Fernando", 4, 5);
      
      List<Aluno> lista = new ArrayList<>();
      lista.add(jose);
      lista.add(felipy);
      lista.add(fernando);
      
      Boletim boletim = new Boletim();
      int resultado = boletim.calculoDeAlunos(lista);
        assertEquals(1 ,resultado, 0.1);
        
    }
    
}
